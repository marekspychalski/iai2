var collapsed=true;

$(document).ready(function () {
    
    $("#logInBtn").click(function(){
        if(collapsed){
            $("#sidebar").load("helper/form.html", function() {
                formValiate();
              });
            $( "#sidebar").switchClass( "out", "in", 500, "easeInOutQuad" );
            collapsed=false;
        }else{
            $( "#sidebar").switchClass( "in", "out", 500, "easeInOutQuad" );
            collapsed=true;
        }
    });
    
    
    
});

function formValiate(){
    
    $(".fa-close").click(function(){
        $( "#sidebar").switchClass( "in", "out", 500, "easeInOutQuad" );
        collapsed=true;
    });
    
    var loginForm = $("#loginForm");
    loginForm.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            login: {
                minlength: 3,maxlength: 32,required: true
            },
            password: {
                maxlength: 32,minlength: 3,required: true
            },

        },
        invalidHandler: function (event, validator) { //display error alert on form submit
        },
        highlight: function (element) {
            $(element)
                .closest('.help-block').removeClass('ok'); // OK 
            $(element)
                .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function (label) {
            label
                .addClass('valid').addClass('help-block ok') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
        },
        submitHandler: function (loginForm) {
            $.ajax({ // ajax query to validate
                url: "controller/index.php?login="+$('#login').val()+"&&pass="+$('#password').val(),
                dataType:"json",
                async: false,
                type: "GET",
                cache:false,
                success : function(responseText) {
                    bootbox.alert({
                         title: "Logowanie pomyślne",
                         message: "Witaj <b>"+responseText.login+"!</b>",
                         size: 'small'
                    });
                   
                },complete : function(responseText) {
                    $("#logInBtn").trigger('click');
                }
            })
        }    
    });
}